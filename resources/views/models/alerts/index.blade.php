@extends('adminlte::page')

@section('content')

    <section class="content-header">
        <h1 class="pull-left">Alertas</h1>
    </section>

    <div class="container p-0">
        <div class="row">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane bg-white fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <table class="table table-striped">
                        <thead>
                        <tr class="tab-header">
                            <th scope="col text-uppercase">Hora de evento</th>
                            <th scope="col text-uppercase">Ultima modificacion</th>
                            <th scope="col text-uppercase">Ver Alerta</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($table))
                            @foreach($table as $row)
                                <tr>
                                    <td>{{ $row->created_at }}</td>
                                    <td>{{ $row->updated_at }}</td>
                                    <td>
                                        <div class='btn-group'>
                                            <a href="{!! route('alerts.show', [$row->id]) !!}"
                                               class='btn btn-default btn-see btn-xs'>
                                                <i class="far fa-eye"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div>
            {{ $table->links() }}
        </div>
    </div>








@endsection
