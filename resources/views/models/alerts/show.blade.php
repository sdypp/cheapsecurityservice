@extends('adminlte::page')

@section('content')

    <section class="content-header">
        <h1 class="pull-left">Información de Alertas</h1>
    </section>

    <div class="container p-0">
        <div class="row">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane bg-white fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <table class="table table-striped">
                        <thead>
                        <tr class="tab-header">
                            <th scope="col text-uppercase">Media</th>
                            <th scope="col text-uppercase">Fecha</th>
                            <th scope="col text-uppercase">Camera</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($table))
                            @foreach($table as $key => $row)
                                <tr>
                                    <td>
                                        <video width="320" height="240" controls>
                                            <source src="{{ $urls[$key] }}" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <source type="video/mp4">
                                    </td>
                                    <td>{{ $row->created_at }}</td>
                                    <td>{{ $row->camera_id }}</td>
                                    {{--                                    <td>--}}
                                    {{--                                        <div class='btn-group'>--}}
                                    {{--                                            <a href="{!! route('alerts.show', [$row->id]) !!}" class='btn btn-default btn-xs'>--}}
                                    {{--                                                <i class="far fa-eye"></i>--}}
                                    {{--                                            </a>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </td>--}}
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('css')
    <link rel="stylesheet" href="css/adminlte.css">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @endsection
