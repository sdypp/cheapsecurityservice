@extends('adminlte::page')

@section('content')

    <section class="content-header">
        <h1 class="pull-left">Locations Edit</h1>
    </section>

    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($object, ['route' => ['locations.update', $object->id], 'method' => 'patch']) !!}

                    @include('models.locations.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
