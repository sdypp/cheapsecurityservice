@extends('adminlte::page')

@section('content')

<div class="Container">
    <div class="card col-5">
        <div class="card-header fz-20">
          Agregar Locación
        </div>
        <div class="box box-primary">
            <div class="box-body">
                <div>
                    {!! Form::open(['route' => 'cameras.store']) !!}

                    @include('models.cameras.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
      </div>
    </div>

@endsection
