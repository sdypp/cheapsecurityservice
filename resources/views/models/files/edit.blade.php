@extends('adminlte::page')

@section('content')

    <section class="content-header">
        <h1 class="pull-left">Files Edit</h1>
    </section>

    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($object, ['route' => ['files.update', $object->id], 'method' => 'patch']) !!}

                    @include('models.files.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
