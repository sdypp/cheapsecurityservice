@extends('adminlte::page')

@section('content')

    <section class="content-header">
        <h1 class="pull-left">Files</h1>

        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px"
               href="{!! route('files.create') !!}">Add New</a>
        </h1>

    </section>



    <div class="card-body table-responsive p-0">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>User Owner</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($table))
                @foreach($table as $row)
                    <tr>
                        <td>{{ $row->id }}</td>
                        <td>{{ $row->path }}</td>
                        <td>{{ $row->camera_id }}</td>
                        <td>{{ $row->created_at }}</td>
                        <td>{{ $row->deleted_at }}</td>
                        <td>
                            {!! Form::open(['route' => ['files.destroy', $row], 'method' => 'delete']) !!}
                            <div class='btn-group'>
                                <a href="{!! route('files.show', [$row]) !!}" class='btn btn-default btn-xs'>
                                    <i class="far fa-eye"></i>
                                </a>
                                <a href="{!! route('files.edit', [$row]) !!}" class='btn btn-default btn-xs'>
                                    <i class="fas fa-edit"></i>
                                </a>
                                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>',
                                ['type' => 'submit', 'class' => 'fas fa-trash-alt',
                                 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                            {!! Form::close() !!}

                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @if(isset($table))
            <div>
                {{ $table->links() }}
            </div>
        @endif
    </div>

@endsection
