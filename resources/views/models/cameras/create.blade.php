@extends('adminlte::page')

@section('content')

    <section class="content-header">
        <h1 class="pull-left">Cameras Create</h1>
    </section>

    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'cameras.store']) !!}

                    @include('models.cameras.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>



@endsection
