@extends('adminlte::page')

@section('content')

    <section class="content-header">
        <h1 class="pull-left">Cameras Edit</h1>
    </section>

    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($object, ['route' => ['cameras.update', $object->id], 'method' => 'patch']) !!}

                    @include('models.cameras.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@endsection
