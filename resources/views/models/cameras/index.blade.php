@extends('adminlte::page')

@section('content')

    <section class="content-header">
        <h1 class="pull-left ">Cámaras</h1>
    </section>


    <div class="container p-0">
        <div class="row">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane bg-white fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <table class="table table-striped">
                        <thead>
                        <tr class="tab-header">
                            <th scope="col text-uppercase">ID</th>
                            <th scope="col text-uppercase">Name</th>
                            <th scope="col text-uppercase">Location id</th>
                            <th scope="col text-uppercase">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($table))
                            @foreach($table as $row)
                                <tr>
                                    <td>{{ $row->id }}</td>
                                    <td>{{ $row->name }}</td>
                                    <td>{{ $row->location_id }}</td>
                                    <td>
                                        {!! Form::open(['route' => ['cameras.destroy', $row], 'method' => 'delete']) !!}
                                        <div class='btn-group'>
{{--                                            <a href="{!! route('cameras.show', [$row]) !!}"--}}
{{--                                               class='btn btn-default btn-xs'>--}}
{{--                                                <i class="far fa-eye"></i>--}}
{{--                                            </a>--}}
{{--                                            <a href="{!! route('cameras.edit', [$row]) !!}"--}}
{{--                                               class='btn btn-default btn-xs'>--}}
{{--                                                <i class="fas fa-edit"></i>--}}
{{--                                            </a>--}}
                                            {!! Form::button('<i class="glyphicon glyphicon-trash "></i>',
                                            ['type' => 'submit', 'class' => 'fas fa-trash-alt btn-delete',
                                             'onclick' => "return confirm('Are you sure?')"]) !!}
                                        </div>
                                        {!! Form::close() !!}

                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div>
            {{ $table->links() }}
        </div>

        <section class="col-12 text-right">
            <h1 class="pull-right">
                <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px"
                   href="#popupCameras">Agregar Cámara</a>
            </h1>
        </section>

        <div id="popupCameras" class="overlay">

            <div class="card col-5 popupCameras">
                <div class="card-header fz-20">
                    Agregar Cámara
                </div>
                <div class="box box-primary">
                    <div class="box-body">
                        <div>
                            {!! Form::open(['route' => 'cameras.store']) !!}

                            @include('models.cameras.fields')

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection
