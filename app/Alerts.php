<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alerts extends Model
{

    protected $table = 'alerts';
    protected $fillable = [
        'user_id',
        'viewed',
        'created_at',
        'updated_at'
    ];
}
