<?php


namespace App\Buisness;


use App\Alerts;
use App\Cameras;
use App\FileAlert;
use App\Files;
use Illuminate\Support\Facades\Log;

class AlertManager
{

    public function proccesAlert(string $file_name, int $camera_id, string $created_at){
        $camera = Cameras::find($camera_id);
        if($camera != null){

            $file = new Files();
            $file->path = $file_name;
            $file->camera_id = $camera->id;
            $file->save();

            $date = date("Y-m-d H:i:s", strtotime($created_at));
            $alert = $this->_isNewAlertCase($camera->user_id, $date);
            if($alert == 0){
                //Nueva Alerta
                $this->_createNewAlert($file->id, $camera->user_id, $date);

            }else{
                //alerta ya existente
                $this->_addToAlert($file->id, $alert, $date);
            }
        }
    }


    private function _createNewAlert($file_id, $user_id, $alert_time){
        $alert = new Alerts();
        $alert->user_id = $user_id;
        $alert->updated_at = $alert_time;
        $alert->created_at = $alert_time;
        $alert->save();

        $fileAlert = new FileAlert();
        $fileAlert->alert_id = $alert->id;
        $fileAlert->file_id = $file_id;
        $fileAlert->save();
    }

    private function _addToAlert($file_id, $alert_id, $alert_time){
        $alert = Alerts::find($alert_id);
        $alert->updated_at = $alert_time;
        $alert->save();

        $fileAlert = new FileAlert();
        $fileAlert->alert_id = $alert_id;
        $fileAlert->file_id = $file_id;
        $fileAlert->save();


    }

    private function _isNewAlertCase(int $user_id, string $created_at){
        Log::debug($created_at);
        $start = date('Y-m-d H:i:s',strtotime('-1 minutes',strtotime($created_at)));
        $end = $created_at;

        $alerts = Alerts::query()
            ->where('user_id', '=', $user_id)
            ->whereBetween('updated_at', [$start, $end])
            ->orderBy('updated_at', 'DESC')->first();

        if(is_null($alerts) || empty($alerts)){
            return 0;
        }else{
            return $alerts->id;
        }
    }


}
