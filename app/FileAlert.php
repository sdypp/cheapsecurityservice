<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileAlert extends Model
{

    public $timestamps = false;

    protected $table = 'file_alert';
    protected $fillable = [
        'alert_id',
        'file_id'
    ];
}
