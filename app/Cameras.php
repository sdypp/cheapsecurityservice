<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cameras extends Model
{
    protected $table = 'cameras';
    protected $fillable = [
        'name',
        'location_id',
        'user_id'
    ];
}
