<?php

namespace App\Http\Controllers;

use App\Cameras;
use App\Locations;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CamerasController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Cameras::query();
        $query = $this->queryFilter($request, $query);
        $table = $query->paginate(15);
        return view('models.cameras.index')
            ->with('table', $table);
    }

    protected function queryFilter(Request $request, Builder $query): Builder
    {
        $query->where('user_id', '=', Auth::user()->id);

        return $query;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        return view('models.cameras.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input = array_merge($input, ['user_id' => Auth::user()->id]);

        $location = Locations::query()->newQuery()
            ->where('id', '=', $input['location_id'])
            ->where('user_id', '=', Auth::user()->id)->get()->toArray();


        if(is_null($location) || empty($location)){
            return redirect(route('cameras.index'));
        }

        Cameras::create([
            'name' => $input['name'],
            'location_id' => $input['location_id'],
            'user_id' => $input['user_id']
        ]);

        return redirect(route('cameras.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Cameras $cameras
     * @return void
     */
    public function show(Request $request, $cameras)
    {
        $findResult = Cameras::find($cameras);

        if (empty($findResult)) {
            return redirect(route('cameras.index'));
        }
        return view('models.cameras.show')
            ->with('object', $findResult);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Cameras $cameras
     * @return void
     */
    public function edit(Request $request, $cameras)
    {
        $findResult = Cameras::find($cameras);

        if (empty($findResult)) {
            return redirect(route('cameras.index'));

        }

        return view('models.cameras.edit')
            ->with('object', $findResult);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Cameras $cameras
     * @return Response
     */
    public function update(Request $request, $cameras)
    {
        $objToUpdate = Cameras::find($cameras);

        if (empty($objToUpdate)) {
            return redirect(route('cameras.index'));
        }

        $input = $request->all();

        $objToUpdate->update($input);

        return redirect(route('cameras.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Cameras $cameras
     * @return void
     */
    public function destroy(Request $request, $cameras)
    {
        $findResult = Cameras::find($cameras);

        if (empty($findResult)) {
            return redirect(route('cameras.index'));
        }

        Cameras::destroy($findResult->id);

        return redirect(route('cameras.index'));
    }
}
