<?php


namespace App\Http\Controllers\API;

use App\Buisness\AlertManager;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StoreImage extends Controller
{

    public function Store(Request $request)
    {
        if(!$request->has('file_path') && !$request->has('cam_id') && !$request->has('created_at'))
            return response('', 500);

        (new AlertManager())->proccesAlert($request->get('file_path'),$request->get('cam_id'),$request->get('created_at'));

        return response('Stored Successfully', 200);
    }

}
