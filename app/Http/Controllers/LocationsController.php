<?php

namespace App\Http\Controllers;

use App\Locations;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class LocationsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $query = Locations::query();
        $query = $this->queryFilter($request, $query);
        $table = $query->paginate(15);
        return view('models.locations.index')
            ->with('table', $table);
    }

    protected function queryFilter(Request $request, Builder $query): Builder
    {
        $query->where('user_id', '=', Auth::user()->id);

        return $query;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function create(Request $request)
    {
        return view('models.locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input = array_merge($input, ['user_id' => Auth::user()->id]);

        Locations::create($input);

        return redirect(route('locations.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Locations $locations
     * @return Factory|View
     */
    public function show(Request $request, $locations)
    {
        $findResult = Locations::find($locations);

        if (empty($findResult)) {
            return redirect(route('locations.index'));
        }
        return view('models.locations.show')
            ->with('object', $findResult);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Locations $locations
     * @return \Illuminate\Http\RedirectResponse|Redirector
     */
    public function edit(Request $request, $locations)
    {
        $findResult = Locations::find($locations);

        if (empty($findResult)) {
            return redirect(route('locations.index'));

        }

        return view('models.locations.edit')
            ->with('object', $findResult);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Locations $locations
     * @return Response
     */
    public function update(Request $request, $locations)
    {
        $objToUpdate = Locations::find($locations);

        if (empty($objToUpdate)) {
            return redirect(route('locations.index'));
        }

        $input = $request->all();

        $objToUpdate->update($input);

        return redirect(route('locations.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Locations $locations
     * @return void
     */
    public function destroy(Request $request, $locations)
    {
        $findResult = Locations::find($locations);

        if (empty($findResult)) {
            return redirect(route('locations.index'));
        }

        Locations::destroy($findResult->id);

        return redirect(route('locations.index'));
    }
}
