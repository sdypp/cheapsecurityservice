<?php

namespace App\Http\Controllers;

use App\Files;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FilesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Files::query();
        $query = $this->queryFilter($request, $query);
        $table = $query->paginate(15);
        return view('models.files.index')
            ->with('table', $table);
    }

    protected function queryFilter(Request $request, $query): Builder
    {
        return $query;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        return view('models.files.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        Files::create($input);

        return redirect(route('files.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Files $files
     * @return void
     */
    public function show(Request $request, $files)
    {
        $findResult = Files::find($files);

        if (empty($findResult)) {
            return redirect(route('files.index'));
        }
        return view('models.files.show')
            ->with('object', $findResult);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Files $files
     * @return void
     */
    public function edit(Request $request, $files)
    {
        $findResult = Files::find($files);

        if (empty($findResult)) {
            return redirect(route('files.index'));

        }

        return view('models.files.edit')
            ->with('object', $findResult);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Files $files
     * @return Response
     */
    public function update(Request $request, $files)
    {
        $objToUpdate = Files::find($files);

        if (empty($objToUpdate)) {
            return redirect(route('files.index'));
        }

        $input = $request->all();

        $objToUpdate->update($input);

        return redirect(route('files.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Files $files
     * @return void
     */
    public function destroy(Request $request, $files)
    {
        $findResult = Files::find($files);

        if (empty($findResult)) {
            return redirect(route('files.index'));
        }

        Files::destroy($findResult->id);

        return redirect(route('files.index'));
    }
}
