<?php


namespace App\Http\Controllers;


use App\Alerts;
use App\FileAlert;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AlertsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $query = Alerts::query();
        $query = $this->queryFilter($request, $query);
        $table = $query->paginate(15);

        return view('models.alerts.index')
            ->with('table', $table);
    }

    protected function queryFilter(Request $request, Builder $query): Builder
    {
        $query->where('user_id', '=', Auth::user()->id);

        $query->orderByDesc('created_at');

        return $query;
    }

    public function show(Request $request, $alert)
    {
        $findResult = FileAlert::query()->where('alert_id', '=', $alert)
            ->join('files', 'file_id', '=', 'files.id')->get();

        if (empty($findResult)) {
            return redirect(route('alerts.index'));
        }
        $urls = [];
        foreach ($findResult as $r) {
            $url = Storage::temporaryUrl(
                $r->path, now()->addMinutes(5)
            );
            array_push($urls, $url);
        }

        return view('models.alerts.show')
            ->with('table', $findResult)
            ->with('urls', $urls);
    }


}
